const path = require('path') // gestion fichiers locaux
const express = require('express') //framework mvc
const nunjucks = require('nunjucks') // templates
const session = require('express-session') // sessions
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const { count } = require('console')

//const { express } = require("./config");

const config = require(path.join(__dirname, 'config.js'))

const MatchSchema = new mongoose.Schema({
    date: { type: Date, required: true },
    heure: { type: String, required: true },
    lieu: { type: String, required: true },
    butsA: { type: Number, required: false },
    butsB: { type: Number, required: false },
    checked: { type: Boolean, required: true },
    clubA: { type: String, required: true },
    clubB: { type: String, required: true },
    compte: { type: String, required: true }
})

const ClubSchema = new mongoose.Schema({
    nom: { type: String, required: true }
})

const CompteSchema = new mongoose.Schema({
    email: { type: String, required: true },
    mdp: { type: String, required: true}
})

/*mongoose.set('useFindAndModify', false);
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useCreateIndex', true);*/

const Match = mongoose.model('Match', MatchSchema)
const Club = mongoose.model('Club', ClubSchema)
const Compte = mongoose.model('Compte', CompteSchema)

mongoose.connect('mongodb://' + config.mongodb.host + '/' + config.mongodb.db)
mongoose.connection.on('error', err => {
    console.error(err)
})

let app = express()

nunjucks.configure('views', {
    express: app
})
  
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

let sessionStore = new session.MemoryStore()

app.use(express.static(path.join(__dirname, '/views')))
app.use(session({
    cookie: { maxAge: 3600000 },// 1 heure
    store: sessionStore,
    saveUninitialized: false,
    resave: false,
    secret: config.express.cookieSecret
}))

app.use((req, res, next) => {
    next()
})

let router = express.Router()

let lesClubs = [
    //new Club({nom:'FC Barcelone'}).save().then(c=>{}).catch(err=>{console.warn(err)}),
    'FC Barcelone', 'Real Madrid', 'Atlético Madrid', 'Séville',
    'Paris Saint Germain', 'Olympique de Marseille', 'Olympique Lyonnais', 'LOSC',
    'Chelsea', 'Liverpool', 'Manchester City', 'Manchester United',
    'Inter Milan', 'Juventus Turin', 'AC Milan', 'Naples',
    'Borussia Dortmund', 'Bayern Munich', 'Bayer Leverkusen', 'RB Leipzig',
    'SL Benfica', 'Sporting CP', 'FC Porto', 'Sporting Braga',
    'Ajax Amsterdam', 'Galatasaray', 'Celtic Glasgow', 'Dinamo Zagreb'
]

router.route('/').get(
    (req, res) => {
        Match.find().then(matchs => {

            if (typeof req.session.compte!=='undefined' && req.session.compte!=null)
                compte = req.session.compte
            else 
                compte = null


            matchs_no_chk = []
            matchs_chk = []
            lesMatchs = []

            if (compte!=null) {

                matchs.forEach( function(m) {

                    if (m.compte==compte._id) {
                        lesMatchs.push(m)
                        if(!m.checked)
                            matchs_no_chk.push(m)
                        else
                            matchs_chk.push(m)
                    }

                })

            }

            res.render('index.njk', {
                matchs: lesMatchs,
                matchs_no_chk: matchs_no_chk,
                matchs_chk: matchs_chk,
                lesClubs: lesClubs,
                compte: compte
            })

        }).catch(err => {
            console.error(err)
        })
    }
)

router.route('/add').post(
    (req, res) => {

        if (req.session.compte!=null) {
            Match.find().then(matchs => {

                if (req.body.inputButsA!="" && !isNaN(req.body.inputButsA) && req.body.inputButsB!="" && !isNaN(req.body.inputButsB)) {
                    varButsA=parseInt(req.body.inputButsA)
                    varButsB=parseInt(req.body.inputButsB)
                } else {
                    varButsA=null
                    varButsB=null
                }

                new Match({
                    date: new Date(req.body.inputDate),
                    heure: req.body.inputHeure,
                    lieu: req.body.inputLieu,
                    butsA: varButsA,
                    butsB: varButsB,
                    checked: false,
                    clubA: req.body.inputClubA,
                    clubB: req.body.inputClubB,
                    compte: req.session.compte._id
                }).save().then(match => {
                    console.log('Votre match a été ajouté');
                    res.redirect('/matchs')
                }).catch(err => {
                    console.warn(err);
                })

            }).catch(err => {
                console.error(err)
            })
        } else {
            res.redirect('/matchs')
        }
    }
)

router.route('/edit/:id').get(
    (req, res) => {

        if (req.session.compte!=null) {
            Match.findById(req.params.id).then(m => {
                if (!m.checked)
                    res.render('edit.njk', { m: m, lesClubs: lesClubs })
                else
                    res.redirect('/matchs')
            }).catch(err => {
                console.error(err)
            })
        } else {
            res.redirect('/matchs')
        }

    }
).post(
    (req, res) => {

        if (req.session.compte!=null) {
            id = req.params.id;
            Match.findByIdAndRemove(id).then(() => {
                
                if (req.body.inputButsA!="" && !isNaN(req.body.inputButsA) && req.body.inputButsB!="" && !isNaN(req.body.inputButsB)) {
                    varButsA=parseInt(req.body.inputButsA)
                    varButsB=parseInt(req.body.inputButsB)
                } else {
                    varButsA=null
                    varButsB=null
                }

                new Match({
                    _id: id,
                    date: new Date(req.body.inputDate),
                    heure: req.body.inputHeure,
                    lieu: req.body.inputLieu,
                    butsA: varButsA,
                    butsB: varButsB,
                    checked: false,
                    clubA: req.body.inputClubA,
                    clubB: req.body.inputClubB,
                    compte: req.session.compte._id
                }).save().then(match => {
                    console.log('Votre match a été modifié');
                    res.redirect('/matchs')
                }).catch(err => {
                    console.warn(err);
                })
                
            }).catch(err => {
                console.error(err)
            })
        } else {
            res.redirect('/matchs')
        }

    }
)

router.route('/delete/all').get(
    (req, res) => {

        Match.find().then(matchs => {
            
            matchs.forEach( function(m) {
                if (m.checked) {

                    Match.findByIdAndRemove(m._id).then(() => {
                    }).catch(err => {
                        console.error(err)
                    })

                }
            })
            
            console.log('La liste secondaire a été vidé');
            res.redirect('/matchs')

        }).catch(err => {
            console.error(err)
        })

    }
)

router.route('/delete/:id').get(
    (req, res) => {

        Match.findById(req.params.id).then(m => {

            if (m.checked) {
                Match.findOneAndRemove({ _id: req.params.id }).then(() => {
                    console.log('Votre match a été supprimé');
                    res.redirect('/matchs')
                }).catch(err => {
                    console.error(err)
                })
            } else {
                res.redirect('/matchs')
            }

        }).catch(err => {
            console.error(err)
        })

    }
)

router.route('/check/:id').get(
    (req, res) => {

        if (req.session.compte!=null) {
            Match.findById(req.params.id).then(m => {

                if (!m.checked) {

                    id = m._id;
                    date = m.date;
                    heure = m.heure;
                    lieu = m.lieu;
                    butsA = m.butsA;
                    butsB = m.butsB;
                    clubA = m.clubA;
                    clubB = m.clubB;
                    compte = m.compte;

                    Match.findByIdAndRemove(id).then(() => {

                        new Match({
                            _id: id,
                            date: date,
                            heure: heure,
                            lieu: lieu,
                            butsA: butsA,
                            butsB: butsB,
                            checked: true,
                            clubA: clubA,
                            clubB: clubB,
                            compte: compte
                        }).save().then(match => {
                            console.log('Votre match a été checké');
                            res.redirect('/matchs')
                        }).catch(err => {
                            console.warn(err);
                        })
                        
                    }).catch(err => {
                        console.error(err)
                    })

                } else {
                    res.redirect('/matchs')
                }

            }).catch(err => {
                console.error(err)
            })
        } else {
            res.redirect('/matchs')
        }

    }
)

router.route('/login').post(
    (req, res) => {

        Compte.find().then(comptes => {

            exist = false
            compte = null
            comptes.forEach( function(c) {
                if (c.email==req.body.inputEmail) {
                    exist = true
                    compte = c
                }
            })

            if (exist && compte!=null && req.body.inputMdp==compte.mdp)
                req.session.compte = compte

            else if (req.body.inputMdp!=compte.mdp)
                console.log('Mot de passe incorrecte')

            res.redirect('/matchs')

        }).catch(err => {
            console.error(err)
        })

    }
)

router.route('/signin').post(
    (req, res) => {

        if (req.body.inputMdp!=req.body.inputMdpConfirm) {
            res.redirect('/matchs')
        } else {
            Compte.find().then(comptes => {

                exist = false
                comptes.forEach( function(c) {
                    if (c.email==req.body.inputEmail)
                        exist = true
                })
                if (exist) {
                    res.redirect('/matchs')
                } else {

                    new Compte({
                        email: req.body.inputEmail,
                        mdp: req.body.inputMdp
                    }).save().then(match => {
                        console.log('Votre compte a été ajouté');
                        res.redirect('/matchs')
                    }).catch(err => {
                        console.warn(err);
                    })

                }

            }).catch(err => {
                console.error(err)
            })
        }

    }
)

router.route('/logout').get(
    (req, res) => {

        if (req.session.compte!=null)
            req.session.compte = null

        res.redirect('/matchs')
        
    }
)

app.use('/matchs', router)
app.use('/pub', express.static('public'))
app.use((req, res) => {
    res.redirect('/matchs')
})

app.listen(config.express.port, config.express.ip, () => {
    console.log('Server listening on ' + config.express.ip + ':' + config.express.port)
})