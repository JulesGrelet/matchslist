module.exports = {
    mongodb: {
      host: '127.0.0.1',
      port:'27017',
      db: 'matchs'
    },
    express: {
      cookieSecret: 'secret',
      port: 3000,
      ip: '127.0.0.1'
    }
  }
  