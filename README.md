const MatchSchema = new mongoose.Schema({
    id: { type: Number, required: true },
    date: { type: Date, required: true },
    heure: { type: String, required: true },
    lieu: { type: String, required: true },
    butsA: { type: Number, required: true },
    butsB: { type: Number, required: true },
    checked: { type: Boolean, required: true },
    clubsA: { type: Number, required: true },
    clubsB: { type: Number, required: true },
    compte: { type: Number, required: true }
})

const ClubSchema = new mongoose.Schema({
    id: { type: Number, required: true },
    nom: { type: String, required: true },
    logo: { type: String, required: true } // (?)
})

const CompteSchema = new mongoose.Schema({
    id: { type: Number, required: true },
    email: { type: String, required: true },
    mdp: { type: String, required: true }
})